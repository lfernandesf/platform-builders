package br.com.test.controller;

import br.com.test.domain.Client;
import br.com.test.service.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class Clientcontroller {

    private static final Logger log = LoggerFactory.getLogger(Clientcontroller.class);

    @Autowired
    private ClientService clientService;

    @GetMapping("/clients")
    public ResponseEntity<List<Client>> getClientsByParameter(
            @RequestParam(value="cpf", required = false) String cpf,
            @RequestParam(value="name", required = false) String name,
            @RequestParam(value="order", required=false) String order,
            Pageable pageable) {

        log.info("Rest searching clients...");
        return new ResponseEntity<List<Client>>(clientService.find(cpf, name, order, pageable), HttpStatus.OK);
    }

    @PostMapping(value="/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Client> add(@RequestBody Client client){

        log.info("Rest adding client...");
        return new ResponseEntity<Client>(clientService.add(client), HttpStatus.OK);
    }

    @PutMapping(value="/clients/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Client> updateStudent(@RequestBody Client client, @PathVariable Integer id) {

        log.info("Rest updating client...");
        Client clientUpdated = clientService.update(client,id);
        if(clientUpdated != null){
            return new ResponseEntity<>(clientUpdated, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(client, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/clients/{id}")
    public ResponseEntity<HttpStatus> deleteStudent(@PathVariable Integer id) {
        log.info("Rest deleting client...");

        try{
            clientService.delete(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PatchMapping("/clients/{clientId}")
    public ResponseEntity<Void> updatePatch(@PathVariable Integer clientId,
                                              @RequestBody Client client) {
        log.info("Rest updating client...");
        Client clientUpdated = clientService.update(client,clientId);
        if(clientUpdated != null){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }
}
