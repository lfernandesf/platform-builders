package br.com.test.service.impl;

import br.com.test.domain.Client;
import br.com.test.repository.ClientCustomRepository;
import br.com.test.repository.ClientRepository;
import br.com.test.service.ClientService;
import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class ClientServiceImpl implements ClientService {

    private static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientCustomRepository clientCustomRepository;

    @Override
    public List<Client> find(String cpf, String name, String order, Pageable pageable){
        log.info("Searching clients...");
        return clientCustomRepository.find(name, cpf, order, pageable);
    }

    @Override
    public Client add(Client client){
        log.info("Saving the new client...");
        return clientRepository.save(client);
    }

    @Override
    public Client update(Client client, Integer id){
        log.info("Updating the new client...");

        Optional<Client> clientData = clientRepository.findById(id);
        if(clientData.isPresent()){
            Client _client = clientData.get();
            _client.setCpf(client.getCpf());
            _client.setName(client.getName());
            _client.setDateBirth(client.getDateBirth());
            return clientRepository.save(_client);
        }
        return null;
    }

    @Override
    public void delete(Integer id){
        log.info("Deleting the client...");
        clientRepository.deleteById(id);;
    }

    public static void main(String [] args){

        Faker faker = new Faker();

        for(int i=1; i<=103; i++){
            String name = faker.name().fullName();
            Date date = dateFake();
            String cpf = cpf(false);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            System.out.println("INSERT INTO client (id,cpf,date_birth,name) VALUES ("+i+",'"+cpf+"','"+sdf.format(date)+"','"+name+"');");
        }

    }

    private static Date dateFake(){
        Random random = new Random();
        int minDay = (int) LocalDate.of(1900, 1, 1).toEpochDay();
        int maxDay = (int) LocalDate.of(2010, 1, 1).toEpochDay();
        long randomDay = minDay + random.nextInt(maxDay - minDay);

        LocalDate randomBirthDate = LocalDate.ofEpochDay(randomDay);

        return Date.from(randomBirthDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    private static int randomiza(int n) {
        int ranNum = (int) (Math.random() * n);
        return ranNum;
    }

    private static int mod(int dividendo, int divisor) {
        return (int) Math.round(dividendo - (Math.floor(dividendo / divisor) * divisor));
    }

    public static String cpf(boolean comPontos) {
        int n = 9;
        int n1 = randomiza(n);
        int n2 = randomiza(n);
        int n3 = randomiza(n);
        int n4 = randomiza(n);
        int n5 = randomiza(n);
        int n6 = randomiza(n);
        int n7 = randomiza(n);
        int n8 = randomiza(n);
        int n9 = randomiza(n);
        int d1 = n9 * 2 + n8 * 3 + n7 * 4 + n6 * 5 + n5 * 6 + n4 * 7 + n3 * 8 + n2 * 9 + n1 * 10;

        d1 = 11 - (mod(d1, 11));

        if (d1 >= 10)
            d1 = 0;

        int d2 = d1 * 2 + n9 * 3 + n8 * 4 + n7 * 5 + n6 * 6 + n5 * 7 + n4 * 8 + n3 * 9 + n2 * 10 + n1 * 11;

        d2 = 11 - (mod(d2, 11));

        String retorno = null;

        if (d2 >= 10)
            d2 = 0;
        retorno = "";

        if (comPontos)
            retorno = "" + n1 + n2 + n3 + '.' + n4 + n5 + n6 + '.' + n7 + n8 + n9 + '-' + d1 + d2;
        else
            retorno = "" + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + d1 + d2;

        return retorno;
    }
}
