package br.com.test.service;

import br.com.test.domain.Client;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ClientService {

    List<Client> find(String name, String cpf, String order, Pageable pageable);

    Client add(Client client);

    Client update(Client client, Integer id);

    void delete(Integer id);
}
