package br.com.test.repository;

import br.com.test.domain.Client;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ClientCustomRepository {

    List<Client> find(String name, String cpf, String order, Pageable pageable);
}
