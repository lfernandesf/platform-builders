package br.com.test.repository;

import br.com.test.domain.Client;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

@Repository
public class ClientCustomRepositoryImpl implements ClientCustomRepository {

    private static final String ASC = "asc";
    public static final String NAME = "name";
    public static final String DESC = "desc";
    public static final String CPF = "cpf";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Client> find(String name, String cpf, String order, Pageable pageable){

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Client> criteriaQuery = builder.createQuery(Client.class);

        Root<Client> from = criteriaQuery.from(Client.class);
        Predicate predicate = builder.and();

        if(StringUtils.hasText(name)) {
            predicate = builder.like(builder.lower(from.<String>get(NAME)), "%" + name.toLowerCase() + "%");
        }

        if(StringUtils.hasText(cpf)) {
            predicate = builder.and(predicate, builder.equal(from.get(CPF), cpf));
        }

        criteriaQuery = criteriaQuery.select(from).distinct(true).where(predicate);

        String orderCriteria = ASC;
        if (order != null && DESC.equals(order)) {
            orderCriteria = order;
        }

        if(pageable != null && (pageable.getSort() != null && pageable.getSort().isSorted())) {
            Iterator<Sort.Order> orders = pageable.getSort().iterator();
            if(orders.hasNext()){
                Sort.Order orderBy = orders.next();
                criteriaQuery.orderBy(ASC.equals(orderCriteria) ?
                        builder.asc(from.get(orderBy.getProperty())) :
                        builder.desc(from.get(orderBy.getProperty())));
            }
        } else {

            criteriaQuery.orderBy(builder.asc(from.get(NAME)));
        }

        TypedQuery<Client> query = entityManager.createQuery(criteriaQuery);

        if(pageable != null && pageable.getPageNumber() > 0){
            query.setFirstResult(pageable.getPageNumber());
            query.setMaxResults(pageable.getPageSize());
        }

        return query.getResultList();

    }
}
