package br.com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class RestclientApplication {
    public static void main(String[] args) {
        SpringApplication.run(RestclientApplication.class, args);
    }
}
